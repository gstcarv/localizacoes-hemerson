var map,
  markerA,
  markerB,
  destination,
  directionsService,
  directionsDisplay;

initMap();

ouvirLocalizacoes();

// Inicializa o Mapa
function initMap() {
  $.getJSON("./assets/mapstyle.json", function (mapStyle) {
    $.getJSON('./assets/localizacoes.json', function (res) {

      var infoPontoA = res.pontos[0],
        infoPontoB = res.pontos[1];

      // Pontos
      var pointA = new google.maps.LatLng(infoPontoA.localizacao.lat, infoPontoA.localizacao.lng),
        pointB = new google.maps.LatLng(infoPontoB.localizacao.lat, infoPontoB.localizacao.lng)

      destination = pointB;

      // Mapa
      map = new google.maps.Map(document.getElementById('map-canvas'), {
        zoom: 7,
        center: pointA,
        styles: mapStyle,
        disableDefaultUI: true
      });

      // Marcadores

      markerA = new google.maps.Marker({
        title: "point A",
        map: map,
        icon: {
          url: "./assets/markers/" + infoPontoA.imagem,
          scaledSize: new google.maps.Size(30, 40)
        }
      })

      markerB = new google.maps.Marker({
        position: pointB,
        title: "point B",
        map: map,
        icon: {
          url: "./assets/markers/" + infoPontoB.imagem,
          scaledSize: new google.maps.Size(30, 40)
        }
      });

      // Directions
      directionsService = new google.maps.DirectionsService;
      directionsDisplay = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers: true
      });

      var popupPontoA = new google.maps.InfoWindow({
        content: "<h3>Ponto A</h3>"
      }),
      popupPontoB = new google.maps.InfoWindow({
        content: "<h3>Ponto B</h3>"
      });

      popupPontoA.open(map, markerA)
      popupPontoB.open(map, markerB)

      markerA.addListener('click', function(){
        popupPontoA.open(map, markerA)
      })

      markerB.addListener('click', function(){
        popupPontoB.open(map, markerB)
      })


      ouvirUsuario();

    })
  });
}

// Ouve as localizações do JSON
function ouvirLocalizacoes() {

  var tempoLocalizacao = 3000;

  setInterval(function () {
    $.getJSON('./assets/localizacoes.json', function (res) {

      localPontoB = res.pontos[1].localizacao;

      var novaPosB = new google.maps.LatLng(localPontoB.lat, localPontoB.lng),
        lastPosB = markerB.position;

      if ((lastPosB.lat() != novaPosB.lat() || lastPosB.lng() != novaPosB.lng())) {

        var posA = getMarkerLatLng(markerA)

        calculateAndDisplayRoute(posA, novaPosB)
      }

      markerB.setPosition(novaPosB)

    });
  }, tempoLocalizacao)
}

// Cria uma rota
function calculateAndDisplayRoute(pointA, pointB) {
  directionsService.route({
    origin: pointA,
    destination: pointB,
    avoidTolls: true,
    avoidHighways: false,
    travelMode: google.maps.TravelMode.DRIVING
  }, function (response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setMap(map)
      directionsDisplay.setDirections(response);
    } else {
      window.alert('Falha ' + status);
    }
  });
}

// Ouve a localização do Usuário
function ouvirUsuario(){
  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(
      function(localizacao){

        var novaPosicaoA = new google.maps.LatLng(localizacao.coords.latitude, localizacao.coords.longitude);
        markerA.setPosition(novaPosicaoA)

        var posicaoPontoB = getMarkerLatLng(markerB)

        calculateAndDisplayRoute(novaPosicaoA, posicaoPontoB);

      },
      function(){
        alert("Ocorreu um erro ao pegar sua localização")
      }
    )
  } else {
    alert("Seu navegador não suporta localização")
  }
}

function getMarkerLatLng(marker){
 return new google.maps.LatLng(marker.position.lat(), marker.position.lng());
}